# Checkout 51 Coding Challenge

## About the project

This is a full stack application that uses the following technologies:

- Phalcon: A PHP framework
- React: A javascript library for building user interfaces
- MySql: A relational database management system

### Requirements
- Project uses the MVC pattern
    - The Models and Views are in the Phalcon app
    - The Views are in the single page app written in react
- Phalcon uses the Dependency Injection pattern heavily
   - The DI container allowed me to easily inject a sqlite connection for unit tests.
- A Repository for offers was implemented
- The front ends consumes the data via REST API
- The data is parsed and renders it into a list
- Each row contains an image, an name and a cash back value

### Bonuses
- A mysql db is implemented
    - Schema matching the json
    - The build script will populate the data
    - A cli task in phalcon has been implemented to parse the json data from github and populate the offers table
-  Docker compose is used to make the build in one step
    - The application can be run without docker note it will require more steps
- A sorting feature has been implemented in the front end
    - User can sort by name or cash back value
    
## Installation

The application is dockerized, it consists of 3 images:
- Database
- Backend
- Front

To start build and run the containers:

#### `docker-compose up`

To stop the containers:

#### `docker-compose down`

The front end application will be running on local host port 3001

`http://localhost:3001/`

## Unit tests

The unit tests are available once the containers are running

### Back End

#### `docker-compose exec backend ./vendor/phpunit/phpunit/phpunit --coverage-text`

### Front End

#### `docker-compose exec frontend npm run test  -- --watchAll=false --silent --coverage`
