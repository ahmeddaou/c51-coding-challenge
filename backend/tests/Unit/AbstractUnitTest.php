<?php

declare(strict_types=1);

namespace Tests\Unit;

use Phalcon\Di;
use Phalcon\Di\FactoryDefault;
use Phalcon\Incubator\Test\PHPUnit\UnitTestCase;
use PHPUnit\Framework\IncompleteTestError;
use Phalcon\Db\Adapter\Pdo\Sqlite;

abstract class AbstractUnitTest extends UnitTestCase
{
    private bool $loaded = false;

    protected function setUp(): void
    {
        parent::setUp();


        $loader = new \Phalcon\Loader();

        $loader->registerNamespaces(
            [
                'App\Repository' => __DIR__ . '/../../application/app/repository',
                'App\Models' => __DIR__ . '/../../application/app/models',
                'App\Controllers' => __DIR__ . '/../../application/app/controllers',
                'App\Tasks' => __DIR__ . '/../../application/app/tasks',
            ]
        );

        $loader->register();

        $di = new FactoryDefault();

        // Set up the database service
        $di->set(
            'db',
            $this->mockConnection()
        );

        Di::reset();
        Di::setDefault($di);

        $this->loaded = true;
    }

    public function __destruct()
    {
        if (!$this->loaded) {
            throw new IncompleteTestError(
                "Please run parent::setUp()."
            );
        }
    }

    /**
     * @return \Closure
     */
    protected function mockConnection(): \Closure
    {
        $connection = new Sqlite(
            [
                "dbname" => "/tmp/test.sqlite",
            ]
        );
        $connection->execute("
        CREATE TABLE IF NOT EXISTS `offers`
        (
            `id`        INTEGER PRIMARY KEY         NOT NULL ,
            `offer_id`  VARCHAR(45) NOT NULL,
            `name`      VARCHAR(255) NULL,
            `image_url` VARCHAR(255) NULL,
            `cash_back` FLOAT       NOT NULL
        )");
        return function () use ($connection) {
            return $connection;
        };
    }
}