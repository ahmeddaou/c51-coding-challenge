<?php

namespace Tests\Unit\App\Repository;

use App\Repository\Connection;
use Phalcon\Db\Adapter\Pdo\Mysql as PdoMysql;
use Tests\Unit\AbstractUnitTest;

class ConnectionTest extends AbstractUnitTest
{

    protected function setUp(): void
    {
        parent::setUp();
    }


    public function testGetNewConnection()
    {
        $this->assertInstanceOf(PdoMysql::class, Connection::getNewConnection());
    }

    public function testGetSharedConnection()
    {
        $this->assertInstanceOf(PdoMysql::class, Connection::getSharedConnection());
    }

}