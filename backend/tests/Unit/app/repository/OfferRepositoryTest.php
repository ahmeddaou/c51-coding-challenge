<?php

declare(strict_types=1);

namespace Tests\Unit\App\Repository;

use App\Models\Offers;
use App\Repository\OfferRepository;
use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Query\Builder;
use Tests\Unit\AbstractUnitTest;

class OfferRepositoryTest extends AbstractUnitTest
{

    private OfferRepository $offerRepository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->offerRepository = new OfferRepository();
    }

    public function testModelName(): void
    {
        $this->assertEquals(
            $this->offerRepository->getModelName(),
            Offers::class,
        );
    }
    public function testCreate(): void
    {
        $offer = new Offers();
        $offer->offer_id = "39271";
        $offer->name = "Tide Liquid Detergent";
        $offer->image_url = "https://d3bx4ud3idzsqf.cloudfront.net/public/production/4902/56910_1527084051.jpg";
        $offer->cash_back = 1.0;
        $result = $this->offerRepository->create($offer);
        $this->assertTrue($result->success());
    }

    public function testRead(): void
    {
        $result = $this->offerRepository->read();
        $this->assertTrue($result->count() > 1);
    }

    public function testReadById(): void
    {
        $result = $this->offerRepository->readById(0);
        $this->assertTrue(method_exists($this->offerRepository, 'readById'));
    }

    public function testUpdate(): void
    {
        $this->offerRepository->update(new Offers());
        $this->assertTrue(method_exists($this->offerRepository, 'update'));
    }

    public function testDelete(): void
    {
        $this->offerRepository->delete(new Offers());
        $this->assertTrue(method_exists($this->offerRepository, 'delete'));
    }

    public function testcreateQuery(): void
    {
        $result = $this->offerRepository->createQuery('u');
        $this->assertInstanceOf(Builder::class,$result);
    }
}