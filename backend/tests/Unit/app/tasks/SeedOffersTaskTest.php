<?php

namespace Tests\Unit\App\Tasks;

use App\Repository\OfferRepository;
use App\Tasks\SeedOffersTask;
use Tests\Unit\AbstractUnitTest;

class SeedOffersTaskTest extends AbstractUnitTest
{

    protected function setUp(): void
    {
        parent::setUp();
    }


    public function testSeedOffersTask()
    {
        $task = new SeedOffersTask();
        $task->mainAction();
        $offerRepository = new OfferRepository();
        $this->assertTrue($offerRepository->read()->count() > 0);
    }

}

