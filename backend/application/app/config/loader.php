<?php

use App\Repository\Connection;
use App\Repository\OfferRepository;
use Phalcon\Loader;
use Phalcon\Mvc\Micro;
use Phalcon\Di\FactoryDefault;
use Phalcon\Http\Response;
use Phalcon\Mvc\View;

$loader = new Loader();

/**
 * registering namespaces
 */
$loader->registerNamespaces(
    [
        'App\Models' => $config->application->modelsDir,
        'App\Repository' => $config->application->repositoryDir,
        'App\Controllers' => $config->application->controllersDir,
    ]
);


/**
 * We're a registering a set of directories taken from the configuration file
 */
$loader->registerDirs(
    [
        $config->application->controllersDir,
        $config->application->modelsDir
    ]
)->register();

$di = new FactoryDefault();

$di->setShared('view', function () {
    return new View();
});

// Set up the database service
$di->set(
    'db',
    function () {
        return Connection::getNewConnection();
    }
);

// Create and bind the DI to the application
$app = new Micro($di);
$app->before(
    function () use ($app) {

        $origin = $app->request->getHeader("ORIGIN") ? $app->request->getHeader("ORIGIN") : '*';

        $app->response->setHeader("Access-Control-Allow-Origin", $origin)
            ->setHeader("Access-Control-Allow-Methods", 'GET,PUT,POST,DELETE,OPTIONS')
            ->setHeader("Access-Control-Allow-Headers", 'Origin, X-Requested-With, Content-Range, Content-Disposition, Content-Type, Authorization')
            ->setHeader("Access-Control-Allow-Credentials", true);
        $app->response->sendHeaders();
        return true;
    });
// Retrieves all robots
$app->get(
    '/api/offers',
    function () use ($app) {
        $repository = new OfferRepository();
        $response = new Response();
        $response->setJsonContent($repository->read());
        return $response;
    }
);

$app->get(
    '/',
    function () use ($app) {
        return 'Welcome';
    }
);

$app->handle($_SERVER['REQUEST_URI']?? '/');