<?php


namespace App\Repository;


use App\Models\Offers;
use Phalcon\Mvc\Model;

class OfferRepository extends BaseRepository
{

    public function getModelName()
    {
        return Offers::class;
    }

    public function read()
    {
        $modelsManager = $this->getDi()->get('modelsManager');
        $phql = 'SELECT * FROM App\Models\Offers ORDER BY name';
        return $modelsManager->executeQuery($phql);
    }

    public function readById($id)
    {
        // TODO: Implement readById() method.
    }

    public function create(Model $offer)
    {
        $modelsManager = $this->getDi()->get('modelsManager');

        $phql = 'INSERT INTO App\Models\Offers (offer_id, name, image_url, cash_back) VALUES (:offer_id:, :name:, :image_url:, :cash_back:)';

        $result =  $modelsManager->executeQuery(
            $phql,
            [
                'offer_id' => $offer->offer_id,
                'name' => $offer->name,
                'image_url' => $offer->image_url,
                'cash_back' => $offer->cash_back,
            ]
        );

        return $result;
    }

    public function update(Model $model)
    {
        // TODO: Implement update() method.
    }

    public function delete(Model $model)
    {
        // TODO: Implement delete() method.
    }
}