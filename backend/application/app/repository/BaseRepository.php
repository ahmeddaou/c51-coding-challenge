<?php


namespace App\Repository;

use \Phalcon\Mvc\Model;

abstract class BaseRepository extends \Phalcon\DI\Injectable
{
    abstract public function getModelName();

    abstract public function read();
    abstract public function readById($id);
    abstract public function create(Model $model);
    abstract public function update(Model $model);
    abstract public function delete(Model $model);

    /**
     * @param string $alias
     *
     * @return \Phalcon\Mvc\Model\Query\Builder
     */
    public function createQuery($alias)
    {
        return $this->getDi()
            ->get('modelsManager')
            ->createBuilder($alias)
            ->addFrom($this->getModelName(), $alias);
    }
}