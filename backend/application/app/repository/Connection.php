<?php
namespace App\Repository;

use Phalcon\Db\Adapter\Pdo\Mysql as PdoMysql;

class Connection
{
    private static $connection;

    public static function getNewConnection(): PdoMysql
    {
        return self::createConnection();
    }

    public static function getSharedConnection(): PdoMysql
    {
        if (self::$connection === null) {
            self::$connection = self::createConnection();
        }

        return self::$connection;
    }

    private static function createConnection(): PdoMysql
    {
        return new PdoMysql(
            [
                'host'     => getenv('MYSQL_HOST'),
                'username' => getenv('MYSQL_USER'),
                'password' => getenv('MYSQL_PASSWORD'),
                'dbname'   => getenv('MYSQL_DB'),
                'port'     => getenv('MYSQL_PORT'),
            ]
        );
    }

}