<?php
namespace App\Tasks;

use App\Models\Offers;
use Phalcon\Cli\Task;

class SeedOffersTask extends Task
{
    public function mainAction()
    {
        $repository = new \App\Repository\OfferRepository();
        $data = json_decode(file_get_contents('https://raw.githubusercontent.com/checkout51/c51-coding-challenge/master/c51.json'), true);
        foreach($data['offers'] as $o) {
            $offer = new Offers($o);
            $repository->create($offer);
        }
    }
}