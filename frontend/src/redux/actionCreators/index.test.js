import getOffers, { setOfferSort } from './index';
import * as actionType from "../actionTypes";

test('getOffers', () => {
  expect(getOffers()).toEqual({
    type: actionType.GET_OFFERS_REQUESTED,
  });
});


test('setOfferSort', () => {
  expect(setOfferSort({ direction: 'ASC'})).toEqual({
    type: actionType.SET_OFFERS_SORTING,
    sort: {
      direction: 'ASC'
    }
  });
});
