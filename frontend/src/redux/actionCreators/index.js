import * as actionType from '../actionTypes';

export default function getOffers() {
  return {
    type: actionType.GET_OFFERS_REQUESTED,
  };
}

export function setOfferSort(sort) {
  return {
    type: actionType.SET_OFFERS_SORTING,
    sort,
  };
}
