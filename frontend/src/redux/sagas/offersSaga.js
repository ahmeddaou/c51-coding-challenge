import { call, put, takeEvery } from 'redux-saga/effects';
import * as actionType from '../actionTypes';
import retrieveOffers from '../api';

function* fetchOffers() {
  try {
    const offers = yield call(retrieveOffers);
    yield put({ type: actionType.GET_OFFERS_SUCCESS, offers });
  } catch (e) {
    yield put({ type: actionType.GET_OFFERS_FAILED, message: e.message });
  }
}

function* setOffersSortType(action) {
  yield put({ type: actionType.SET_OFFERS_SORTING_SUCCESS, sort: action.sort });
}

function* offerSaga() {
  yield takeEvery(actionType.GET_OFFERS_REQUESTED, fetchOffers);
  yield takeEvery(actionType.SET_OFFERS_SORTING, setOffersSortType);
}

export default offerSaga;
