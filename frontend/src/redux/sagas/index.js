import { all } from 'redux-saga/effects';
import offerSaga from './offersSaga';

export default function* rootSaga() {
  yield all([
    offerSaga(),
  ]);
}
