import axios from 'axios';

export default function retrieveOffers() {
  // `axios` function returns promise, you can use any ajax lib, which can
  // return promise, or wrap in promise ajax call
  return axios.get(`${process.env.REACT_APP_CHECKOUT_51_BACKEND_URL}/api/offers`);
}
