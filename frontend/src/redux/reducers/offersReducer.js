import * as actionTypes from '../actionTypes';

const initialState = {
  offers: [],
  loading: false,
  error: null,
  sort: {
    field: 'name',
    direction: 'asc',
  },
};

export default function offers(state = initialState, action) {
  switch (action.type) {
    case actionTypes.GET_OFFERS_REQUESTED:
      return {
        ...state,
        loading: true,
      };
    case actionTypes.GET_OFFERS_SUCCESS:
      return {
        ...state,
        loading: false,
        offers: action.offers.data,
      };
    case actionTypes.GET_OFFERS_FAILED:
      return {
        ...state,
        loading: false,
        error: action.message,
      };
    case actionTypes.SET_OFFERS_SORTING_SUCCESS:
      return {
        ...state,
        sort: action.sort,
      };
    default:
      return state;
  }
}
