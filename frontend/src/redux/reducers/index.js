import { combineReducers } from 'redux';
import offersReducer from './offersReducer';

const reducer = combineReducers({
  offersReducer,
});

export default reducer;
