import React from 'react';
import { useDispatch } from 'react-redux';
import { Dropdown } from 'semantic-ui-react';
import { setOfferSort } from '../../redux/actionCreators';

const sortOptions = [
  {
    key: 'NAME_ASC',
    value: 'NAME_ASC',
    text: 'name',
    icon: 'arrow up',
    sort: {
      field: 'name',
      direction: 'asc',
    },
  },
  {
    key: 'NAME_DESC',
    value: 'NAME_DESC',
    text: 'name',
    icon: 'arrow down',
    sort: {
      field: 'name',
      direction: 'desc',
    },
  },
  {
    key: 'CASH_BACK_ASC',
    value: 'CASH_BACK_ASC',
    text: 'cash back',
    icon: 'arrow up',
    sort: {
      field: 'cash_back',
      direction: 'asc',
    },
  },
  {
    key: 'CASH_BACK_DESC',
    value: 'CASH_BACK_DESC',
    text: 'cash back',
    icon: 'arrow down',
    sort: {
      field: 'cash_back',
      direction: 'desc',
    },
  },
];

const Sort = () => {
  const dispatch = useDispatch();
  return (
    <Dropdown
      placeholder="Sort By"
      options={sortOptions}
      onChange={(event, result) => {
        const { value } = result || event.target;
        const { sort } = result.options.find((o) => o.value === value);
        dispatch(setOfferSort(sort));
      }}
      fluid
      selection
    />
  );
};

export default Sort;
