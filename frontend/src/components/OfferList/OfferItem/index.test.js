import { render, screen } from '@testing-library/react';
import OfferItem from './index';

test('renders learn react link', () => {
  const mockOffer = {
    name: 'O Lake Butter',
    cash_back: '100.0',
    image_url: 'https://olb.com/a.jpg',
  }
  const { debug } = render(<table><tbody><OfferItem offer={mockOffer} /></tbody></table>);
  // debug();
  expect(screen.getByText(mockOffer.name).closest('a')).toBeVisible();
});
