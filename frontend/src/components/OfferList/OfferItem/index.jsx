import React from 'react';
import PropTypes from 'prop-types';
import { Table } from 'semantic-ui-react';

const OfferItem = ({ offer }) => (
  <Table.Row>
    <Table.Cell><img alt={offer.name} src={offer.image_url} /></Table.Cell>
    <Table.Cell>
      <a href={`https://www.google.com/search?q=${offer.name}&tbm=shop`} target="_blank" rel="noreferrer">{offer.name}</a>
    </Table.Cell>
    <Table.Cell>{`$${Number.parseFloat(offer.cash_back).toFixed(2)}`}</Table.Cell>
  </Table.Row>
);

OfferItem.propTypes = {
  offer: PropTypes.shape({
    name: PropTypes.string,
    cash_back: PropTypes.string,
    image_url: PropTypes.string,
  }),
};

OfferItem.defaultProps = {
  offer: {},
};

export default OfferItem;
