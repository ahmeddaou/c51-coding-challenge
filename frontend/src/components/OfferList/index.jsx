import React from 'react';
import { Table } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import OfferItem from './OfferItem';

const OfferList = ({ offers }) => (
  <Table celled>
    <Table.Header>
      <Table.Row>
        <Table.HeaderCell>Product</Table.HeaderCell>
        <Table.HeaderCell>Name</Table.HeaderCell>
        <Table.HeaderCell>Cash Back Value</Table.HeaderCell>
      </Table.Row>
    </Table.Header>

    <Table.Body>
      {offers.map((o) => <OfferItem key={o.id} offer={o} />)}
    </Table.Body>

  </Table>
);

OfferList.propTypes = {
  offers: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string,
    cash_back: PropTypes.string,
    image_url: PropTypes.string,
  })),
};

OfferList.defaultProps = {
  offers: [],
};

export default OfferList;
