import { render, screen } from '@testing-library/react';
import OfferItem from './index';
import OfferList from './index';

test('renders learn react link', () => {
  const { debug } = render(<OfferList />);
  // debug();
  expect(screen.getByText('Product').closest('th')).toBeVisible()
  expect(screen.getByText('Name').closest('th')).toBeVisible()
  expect(screen.getByText('Cash Back Value').closest('th')).toBeVisible()
});
