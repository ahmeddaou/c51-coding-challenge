import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Container } from 'semantic-ui-react';
import _ from 'lodash';
import getOffers from '../../redux/actionCreators';
import OfferList from '../OfferList';
import Header from '../Header';
import './index.css';
import Sort from '../Sort';

const App = () => {
  const dispatch = useDispatch();
  useEffect(() => dispatch(getOffers()), []);
  const offers = useSelector((state) => state.offersReducer.offers);
  const sort = useSelector((state) => state.offersReducer.sort);
  const data = _.orderBy(offers, [sort.field], [sort.direction]);
  return (
    <div className="App">
      <Header />
      <Container>
        <Sort />
        <OfferList offers={data} />
      </Container>
      <div>
        <a
          className="App-link"
          href="https://checkout51.com"
          target="_blank"
          rel="noopener noreferrer"
        >
          checkout51.com
        </a>
      </div>
    </div>
  );
};

export default App;
