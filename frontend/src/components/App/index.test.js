import {render, screen} from '@testing-library/react';
import {Provider} from 'react-redux';
import Sort from './index';
import offersReducer from './../../redux/reducers/offersReducer';
import {combineReducers, createStore} from "redux";


export function createTestStore() {
  return createStore(
    combineReducers({
      offersReducer,
    })
  );
}

test('renders learn react link', () => {
  const { debug } = render(<Provider store={createTestStore()}><Sort /></Provider>);
  debug()
  expect(screen.getByText('Product').closest('th')).toBeVisible()
  expect(screen.getByText('Name').closest('th')).toBeVisible()
  expect(screen.getByText('Cash Back Value').closest('th')).toBeVisible()
});
