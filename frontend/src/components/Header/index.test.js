import { render, screen } from '@testing-library/react';
import Header from './index';
import PropTypes from "prop-types";

test('renders learn react link', () => {

  const { debug } = render(<Header />);
  // debug();
  expect(screen.getByText('Home').closest('a')).toBeVisible()
});
