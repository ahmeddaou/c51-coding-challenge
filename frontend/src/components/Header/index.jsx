import React, { useState } from 'react';
import { Menu } from 'semantic-ui-react';

const Header = () => {
  // Declare a new state variable, which we'll call "count"
  const [activeItem, setActiveItem] = useState('home');
  const handleItemClick = (e, { name }) => setActiveItem(name);
  return (
    <Menu inverted>
      <Menu.Item
        name="home"
        active={activeItem === 'home'}
        onClick={handleItemClick}
      />
    </Menu>
  );
};

export default Header;
